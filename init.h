#ifndef __init_h
#define __init_h 
//Hal: exp: #define P_led P10 ----------------
#define P_led P1_0
//Const: exp: #define D_data 1 ---------------
#define D_led 1
//Globle var ---------------------------------
#ifdef __init_c
int ledFlashCounter;
#else
extern int ledFlashCounter;
#endif

// macro ===================================
#define F_ledOn() P_led = 1
//Action Macro: exp: #define F_getData() -----

//Function -----------------------------------

#endif