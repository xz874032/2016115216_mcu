1.安装插件：
 （1）C/C++
 （2）C++ Intellisense
 （3）Clang-Format
 （4）hide-gitignored
 （5）Markdown All in One
 （6）vscode-icons
 （7）One Dark Pro

2.点击快捷键：“Ctrl+Shift+P”或点击菜单栏“编辑”—>“Emmet...”打开搜索框，输入“>hide Gitignored...”回车确定就会生成一个“settings.json”文件。

3.在.gitignore文件中，添加“Listings/”和“Objects/”，保存后在“settings.json”文件中查看是否已经添加。


4.“.vscode”里面的{}c_cpp_properties.json是代码自动生成的。通过在includeAll.h里面设置一些不存在的头文件，当其出现一个黄色的小灯泡时，点击它，然后点击“Edit "includePath" setting”即可出现c_cpp_properties.json文件。

5.在c_cpp_properties.json文件中的“defines”选项，添加"sbit=char","sfr=char","bit=char","interrupt 2 ="。保存后会发现“REGtenxTM52F5278B.h”文件里面的错误已经消失了。

6.代码片段自动生成：点击菜单栏的“文件”—>“首选项”—>“用户代码片段”，在弹出的搜索框中选择“C”并回车确定即可看到生成一个“c.json”文件。

7.在其里面配置：
"H file frame":{
   "prefix":"hframe",
   "body":[
      "#ifndef __$1_h",
      "#define __$1_h ",
      "//hal===========\n",
      "//const=====\n",
      "//var=====\n",
      "//function=====\n",


      "#endif"
   ]
},
"C file frame":{
   "prefix":"cframe",
   "body":[
      "#define __$1_c",
      "#include \"includeAll.h\"\n"
   ]
}

8.格式化代码(需要用到插件：Clang-Format)：点击菜单栏中的“文件”—>“首选项”—>“设置”打开“Settings”文件。在扩展—>“C/C++”—>“C_Cpp:Clang_format_fallback Style”选项填写“LLVM”；在扩展—>“Clang-Format configuration”—>Fallback Style选项中填写“LLVM”。
最后，在“Settings”文件中，搜索：“Code Action On Save”，点击“Edit in settings.json”查看一下是否配置成功。也可通过搜索：“C_Cpp:Clang_format_path”，点击“Edit in settings.json”查看一下是否配置成功。

9.局部变量：可能发生动态内存溢出；
  全局变量：在编译的时候就已经分配了内存。
  使用全局变量会增加文件之间的耦合度。
  “extern uint8_t ledFlashCounter”表示其为外部变量，这里只是引用进来进行使用而已。

10.在C语言中，函数一定是全局的，没有局部函数之说，所以函数在引用声明时不需要在前面加上“extern”。

11.变量的作用域：
   静态全局变量的作用域，可见域就是所定义的本文件；别的文件用“extern”是引用不了的，生命周期为整个程序；
   全局变量，用“extern”将其可见域拓展到其他文件。
   静态局部变量：
   局部变量：作用域、可见域、生命周期均为所属函数。

12.“uint8_t ledFlashCounter”:定义并分配内存空间给ledFlashCounter

13.取名字标准
  1）函数名：首字母大写，采用驼峰形式；
  2）变量名：首字母小写，采用驼峰形式；
  3）类型名：基础数据类型全部采用转义的方式，要把其“是否有符号”、“类型形式”、“二进制长度”都表示出来，同时在末尾加上“_t”
   如：typedef unsigned char uint8_t;
       typedef unsigned int uint16_t;
       typedef unsigned long uint32_t;
       typedef char int8_t;
       typedef short int16_t;
   4）宏定义：第一个字母大写，其次加上一个下划线“_”，如：P_led、D_led、F_ledOn()
      以双划线“__”开头的宏定义
      以“P_”开头的宏定义
      以“D_”开头的宏定义  
      以“F_”开头的宏定义

14.main.c文件框架：
