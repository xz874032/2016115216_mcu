#define __main_c
#include "includeAll.h"

//============================================
void main() {
  F_turnOnWDT(); // 看门狗寄存器
  InitSys();

  while (1) {
    F_led1On();
    TimeProcess(2000);
    F_led1Off();
    TimeProcess(2000);
  }
}
//============================================
void InitSys() {
  // system clock
  CLKCON = 0x03;  // Clock div 1
  STPPCK = 0;
  STPFCK = 0;
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  _nop_();
  SELFCK = 1;

  // I/O init
  P2MOD = 0xa;
  
}
//============================================
void DisplayProcess() {}
//============================================
void UserSettingProcess() {}
//============================================
void TaskProcess() {}
//============================================
void TimeProcess(int time) {
	 int i, j;
	 for(i = 0; i < time; i++)
	 for(j = 0; j < 1000; j++);
}
